package com.asseccods.utils;

import com.asseccods.model.Cert;
import com.asseccods.model.User;
import com.google.gson.*;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.swing.text.BadLocationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Mateusz on 04.05.2017.
 */
@Component
public class RestService {

    final static String SERVER_URL="http://213.222.200.96/";
    final static String AUTH = "auth";
    //final static String AUTH_LOGIN="dev_61R49PiM";
    //final static String AUTH_PASSWORD="j5CYuYVm";
    final static String CERT = "cert";
    final static String USER = "user";

    public static String authenticate(String username, String password) throws Exception {
        //try {
            URL url = new URL(SERVER_URL+AUTH);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Authorization",encodeCredentials(username,password));
            httpURLConnection.connect();

            String response = readJson(httpURLConnection);
            JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();

            if(jsonObject.has("status")) throw new Exception();

            return jsonObject.get("token").getAsString();
       /* }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        } */
       // return null;
    }

    private static String encodeCredentials(String username, String password) {
        String credentials=username+":"+password;
        return "Basic "+ Base64.encode(credentials.getBytes());
    }

    private static String readJson(HttpURLConnection httpURLConnection) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

            StringBuilder responseBuilder = new StringBuilder();
            String line;
            while((line = bufferedReader.readLine()) != null) {
                responseBuilder.append(line);
            }

            isHTTPCodeOK(httpURLConnection.getResponseCode());

            return responseBuilder.toString();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static boolean isHTTPCodeOK(int code) {
        return code == 200;
    }

    public static void deauthenticate(HttpSession httpSession) {
        try {
            String token = httpSession.getAttribute("token").toString();
            URL url = new URL(SERVER_URL+AUTH+"/"+token);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("DELETE");
            httpURLConnection.setRequestProperty("Authorization",token);
            httpURLConnection.connect();

            String response = readJson(httpURLConnection);
            JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();

            if(jsonObject.has("status")) throw new Exception();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static User[] getAllUsers(HttpSession httpSession) {
        try {
            URL url = new URL(SERVER_URL+USER);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Authorization","Token "+httpSession.getAttribute("token"));
            httpURLConnection.connect();

            String response = readJson(httpURLConnection);
            Gson gson = new Gson();
            User[] list = gson.fromJson(response, User[].class);

            return list;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean getTokenValid(String token) {
        try {
            URL url = new URL(SERVER_URL+AUTH+"/"+token);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Authorization","Token "+token);
            httpURLConnection.connect();

            String response = readJson(httpURLConnection);

            JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();

            if(jsonObject.get("status").getAsString().equals("OK")) return true;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static User getUser(String username, HttpSession httpSession) {
        try {
            URL url = new URL(SERVER_URL+USER+"/"+username);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Authorization","Token "+httpSession.getAttribute("token"));
            httpURLConnection.connect();

            String response = readJson(httpURLConnection);
            Gson gson = new Gson();
            User user = gson.fromJson(response, User.class);

            return user;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean modifyUser(HttpSession httpSession, User user) {
        try {
            URL url = new URL(SERVER_URL+USER+"/"+user.getUsername());
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("PUT");
            httpURLConnection.setRequestProperty("Authorization","Token "+httpSession.getAttribute("token"));
            httpURLConnection.setRequestProperty("Content-Type","application/json");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();

            Gson gson = new Gson();
            String request = gson.toJson(user);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(request.getBytes("UTF-8"));
            outputStream.close();

            String response = readJson(httpURLConnection);
            JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
            if(jsonObject.get("status").getAsString().equals("OK")) return true;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Cert[] getAllCerts(HttpSession httpSession) {
        try {
            URL url = new URL(SERVER_URL+CERT);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Authorization","Token "+httpSession.getAttribute("token"));
            httpURLConnection.connect();

            String response = readJson(httpURLConnection);
            Gson gson = new Gson();
            return gson.fromJson(response, Cert[].class);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean addCert(HttpSession httpSession, Cert cert) {
        try {
            URL url = new URL(SERVER_URL+CERT);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Authorization","Token "+httpSession.getAttribute("token"));
            httpURLConnection.setRequestProperty("Content-Type","application/json");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();

            Gson gson = new Gson();
            String request = gson.toJson(cert);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(request.getBytes("UTF-8"));
            outputStream.close();

            String response = readJson(httpURLConnection);
            JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
            if(jsonObject.get("status").getAsString().equals("OK")) return true;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean deleteCert(HttpSession httpSession, Integer id) {
        try {
            URL url = new URL(SERVER_URL+CERT+"/"+id);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("DELETE");
            httpURLConnection.setRequestProperty("Authorization","Token "+httpSession.getAttribute("token"));
            httpURLConnection.connect();

            String response = readJson(httpURLConnection);
            JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
            if(jsonObject.get("status").getAsString().equals("OK")) return true;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Cert getCert(Integer id, HttpSession httpSession) {
        try {
            URL url = new URL(SERVER_URL+CERT+"/"+id);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Authorization","Token "+httpSession.getAttribute("token"));
            httpURLConnection.connect();

            String response = readJson(httpURLConnection);
            Gson gson = new Gson();
            return gson.fromJson(response, Cert.class);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
