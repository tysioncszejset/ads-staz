package com.asseccods.model;

import org.cryptacular.util.CertUtil;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mateusz on 07.05.2017.
 */
public class Cert {
    private Integer id;
    private String owner;
    private String raw_bytes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRaw_bytes() {
        return raw_bytes;
    }

    public void setRaw_bytes(String raw_bytes) {
        this.raw_bytes = raw_bytes;
    }

    public Boolean isExpiring() {
        X509Certificate x509Certificate = CertUtil.decodeCertificate(raw_bytes.getBytes());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, +7);
        try {
            x509Certificate.checkValidity(calendar.getTime());
        }
        catch (CertificateExpiredException | CertificateNotYetValidException e) {
            return false;
        }
        return true;
    }

    public Date fetchExpiringDate() {
        X509Certificate x509Certificate = CertUtil.decodeCertificate(raw_bytes.getBytes());
        return x509Certificate.getNotAfter();
    }

    public String fetchCommonName() {
        X509Certificate x509Certificate = CertUtil.decodeCertificate(raw_bytes.getBytes());
        return CertUtil.subjectCN(x509Certificate);
    }
}
