package com.asseccods.helper;

import com.asseccods.model.Cert;
import com.asseccods.model.User;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by Mateusz on 07.05.2017.
 */
public class CertHelper {
    public static Cert[] sortCertsArrayByIdAsc(Cert[] certs) {
        Arrays.sort(certs, Comparator.comparing(Cert::getId));
        return certs;
    }
}
