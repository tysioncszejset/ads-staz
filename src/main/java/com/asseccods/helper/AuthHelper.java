package com.asseccods.helper;

import com.asseccods.utils.RestService;

import javax.servlet.http.HttpSession;

/**
 * Created by Mateusz on 07.05.2017.
 */
public class AuthHelper
{
    public static Boolean isAdminLogged(HttpSession httpSession) {
        Object token = httpSession.getAttribute("token");
        if (token != null && RestService.getTokenValid(token.toString())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

}
