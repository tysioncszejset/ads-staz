package com.asseccods.helper;

import com.asseccods.model.User;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by Mateusz on 05.05.2017.
 */
public class UserHelper {

    public static User[] sortUsersArray(User[] users, String sort) {
        switch (sort) {
            case "username-asc":
                Arrays.sort(users, Comparator.comparing(User::getUsername));
                break;
            case "username-dsc":
                Arrays.sort(users, Comparator.comparing(User::getUsername).reversed());
                break;
            case "lastname-asc":
                Arrays.sort(users, Comparator.comparing(User::getLastName));
                break;
            case "lastname-dsc":
                Arrays.sort(users, Comparator.comparing(User::getLastName).reversed());
                break;
            case "age-asc":
                Arrays.sort(users, Comparator.comparing(User::getAge));
                break;
            case "age-dsc":
                Arrays.sort(users, Comparator.comparing(User::getAge).reversed());
                break;
        }
        return users;
    }

    public static User[] filterUsersArray(User[] users, String filterFirstname, String filterLastname) {

        if(filterFirstname.equals("") && filterLastname.equals("")) return users;

        User[] filteredFirstnameUsers = null;
        if(!filterFirstname.equals("")) {
            filteredFirstnameUsers=Arrays.stream(users).filter(object ->
                    object.getFirstName().toLowerCase()
                            .startsWith(filterFirstname.toLowerCase())).toArray(User[]::new);
        }

        User[] filteredLastnameUsers = null;
        if(!filterLastname.equals("")) {
            filteredLastnameUsers=Arrays.stream(users).filter(object ->
                    object.getLastName().toLowerCase()
                            .startsWith(filterLastname.toLowerCase())).toArray(User[]::new);
        }

        Set<User> combinedFilteredUsers = new LinkedHashSet<>();
        if(filteredFirstnameUsers != null) {
            combinedFilteredUsers.addAll(Arrays.asList(filteredFirstnameUsers));
        }
        if(filteredLastnameUsers != null) {
            combinedFilteredUsers.addAll(Arrays.asList(filteredLastnameUsers));
        }

        return combinedFilteredUsers.toArray(new User[combinedFilteredUsers.size()]);
    }

    public static void setSessionAttributes(Map<String, String> attributes, HttpSession httpSession) {
        attributes.forEach(httpSession::setAttribute);
    }

    public static Boolean isFirstLetterUpper(String string) {
        return string.startsWith(String.valueOf(string.toUpperCase().charAt(0)));
    }
}
