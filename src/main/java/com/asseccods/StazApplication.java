package com.asseccods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class StazApplication {

	public static void main(String[] args) {
		SpringApplication.run(StazApplication.class, args);
	}

}
