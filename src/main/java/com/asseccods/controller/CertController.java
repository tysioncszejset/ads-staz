package com.asseccods.controller;

import com.asseccods.helper.CertHelper;
import com.asseccods.model.Cert;
import com.asseccods.utils.RestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

import static com.asseccods.helper.AuthHelper.isAdminLogged;

/**
 * Created by Mateusz on 04.05.2017.
 */
@Controller
public class CertController {
    @RequestMapping("/list-certs")
    public String listCerts(HttpSession httpSession, Model model) {
        if(!isAdminLogged(httpSession)) return "redirect:login";

        Cert[] certs = RestService.getAllCerts(httpSession);
        certs = CertHelper.sortCertsArrayByIdAsc(certs);

        model.addAttribute("certs", certs);
        return "list-certs";
    }

    @GetMapping("/add-cert")
    public String addCert(HttpSession httpSession, Model model) {
        if(!isAdminLogged(httpSession)) return "redirect:login";
        model.addAttribute("cert",new Cert());
        return "add-cert";
    }

    @PostMapping("/add-cert")
    public String addCertSubmit(HttpSession httpSession, @ModelAttribute Cert cert, Model model) {
        if(!isAdminLogged(httpSession)) return "redirect:login";
        if(!cert.getRaw_bytes().isEmpty() && RestService.addCert(httpSession, cert)) {
            return "list-certs";
        }
        else {
            model.addAttribute("error","Treść certyfikatu nie może być pusta");
            return "redirect:/add-cert";
        }
    }

    @RequestMapping("/delete-cert/{id}")
    public String deleteCert(@PathVariable Integer id, HttpSession httpSession, Model model) {
        if(!RestService.deleteCert(httpSession,id)) {
            model.addAttribute("error","Usuwanie niepomyślne.");
        }
        return "redirect:/list-certs";
    }

    @RequestMapping("download-cert/{id}")
    public String downloadCert(@PathVariable Integer id, HttpSession httpSession, Model model, HttpServletResponse httpServletResponse) {
        httpServletResponse.setContentType("text/pem");
        Cert cert=RestService.getCert(id,httpSession);
        String filename=cert.getOwner()+"_"+Calendar.getInstance().getTime();
        httpServletResponse.setHeader("Content-Disposition", "attachment; filename=\""+filename+".pem\"");
        try
        {
            OutputStream outputStream = httpServletResponse.getOutputStream();
            outputStream.write(cert.getRaw_bytes().getBytes());
            outputStream.flush();
            outputStream.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

}
