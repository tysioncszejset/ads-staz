package com.asseccods.controller;

import com.asseccods.model.Auth;
import com.asseccods.utils.RestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

import static com.asseccods.helper.AuthHelper.isAdminLogged;
import static com.asseccods.utils.RestService.authenticate;

/**
 * Created by Mateusz on 05.05.2017.
 */
@Controller
public class AuthController {
    @GetMapping("/login")
    public String loginForm(Model model, HttpSession httpSession) {
        Auth auth = new Auth();
        Object token = httpSession.getAttribute("token");
        if(token != null && RestService.getTokenValid(token.toString())) {
            auth.setToken(token.toString());
        }
        model.addAttribute("auth", auth);
        return "login";
    }

    @PostMapping("/login")
    public String loginSubmit(@ModelAttribute Auth auth, HttpSession httpSession) {
        try {
            auth.setToken(authenticate(auth.getUsername(), auth.getPassword()));
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.print(e.getMessage());
        }
        httpSession.setAttribute("token",auth.getToken());
        return "login";
    }

    @RequestMapping("/logout")
    public String logout(HttpSession httpSession, Model model) {
        if(!isAdminLogged(httpSession)) return "redirect:login";

        model.addAttribute("auth",new Auth());
        httpSession.removeAttribute("token");
        RestService.deauthenticate(httpSession);
        return "login";
    }
}
