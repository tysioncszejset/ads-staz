package com.asseccods.controller;

import com.asseccods.utils.RestService;
import com.asseccods.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

import static com.asseccods.helper.AuthHelper.isAdminLogged;
import static com.asseccods.helper.UserHelper.*;

/**
 * Created by Mateusz on 04.05.2017.
 */
@Controller
public class UserController {

    @RequestMapping("list-users")
    public String listUsers(@RequestParam(value="sort", defaultValue = "username-asc") String sort, @RequestParam(value="filter-firstname", defaultValue = "") String filterFirstname, @RequestParam(value="filter-lastname", defaultValue = "") String filterLastname, Model model, HttpSession httpSession) {
        if(!isAdminLogged(httpSession)) return "redirect:login";

        model.addAttribute("sort", sort);
        model.addAttribute("filterFirstname", filterFirstname);
        model.addAttribute("filterLastname", filterLastname);

        User[] users = RestService.getAllUsers(httpSession);
        users=filterUsersArray(users, filterFirstname, filterLastname);
        users=sortUsersArray(users, sort);

        model.addAttribute("users", users);
        return "list-users";
    }

    @GetMapping("edit-user/{username}")
    public String editUser(@PathVariable String username, HttpSession httpSession, Model model) {
        if(!isAdminLogged(httpSession)) return "redirect:login";

        User user = RestService.getUser(username, httpSession);
        model.addAttribute("user",user);
        return "edit-user";
    }

    @PostMapping("edit-user/{username}")
    public String editUserSubmit(@PathVariable String username, HttpSession httpSession, @ModelAttribute User user, Model model) {
        if(!isAdminLogged(httpSession)) return "redirect:login";

        if(isFirstLetterUpper(user.getFirstName()) && isFirstLetterUpper(user.getLastName())) {
            Boolean modifyingSucced = RestService.modifyUser(httpSession, user);
            return "redirect:/list-users";
        }
        else {
            model.addAttribute("error","Imię i nazwisko powinny rozpoczynać się od wielkiej litery.");
            return "redirect:/edit-user/{username}";
        }
    }
}
