package com.asseccods;

import com.asseccods.utils.RestService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Created by Mateusz on 04.05.2017.
 */
@Component
public class Runner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments applicationArguments) {
    }
}
